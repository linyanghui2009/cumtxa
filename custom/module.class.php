<?php
class module extends Activity {
	function headerTask(){
		View::displayAsHtml(array(),"tpl/module/header.php");
	}

	function navTask(){
		View::displayAsHtml(array(),"tpl/module/nav.php");
	}

	function footerTask(){
		View::displayAsHtml(array(),"tpl/module/footer.php");
	}
}
?>