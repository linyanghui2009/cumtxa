<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 14-9-25
 * Time: 下午2:23
 */
class newsMode extends Data {
    /** @var  SqlDB */
    protected $db;
    protected $page=1;
    protected $page_size=15;
    protected function __construct(){
        $this->db=SqlDB::init();
    }

    function getList($rows=20,$offset=0,$type=null,$subType=null){
        $rows=(int)$rows;
        $offset=(int)$offset;
        $where="where 1=1";
        if(!empty($type)){
            $type=$this->db->quote($type);
            $where.=" and type=$type ";
        }
        if(!empty($subType)){
            $subType=$this->db->quote($subType);
            $where.=" and subtype=$subType ";
        }
        $sql="select * from news {$where} order by `create_time` desc limit {$offset},{$rows}";
        $data=$this->db->getAll($sql);
        return $data;
    }

    function content($id){

    }
    function subtypeNews(){
        $sql="select `id`,`name`,`pretype` from `news_subtype`";
        return $this->db->getAll($sql);
    }

    function changePage(){
        if(!empty($_GET['page'])){
            $page=(int)$_GET['page'];
        }else{
            $page=$this->page;
        }
        $result['page_begin']=$this->page;
        $result['page_size']=$this->page_size;
        $result['page']=$page;
        $result['news_begin']=($result['page']-1)*$result['page_size'];
        $result['news_total']=$this->db->getValue("select count(*) from news where 1=1");
        $result['page_total']=ceil( $result['news_total']/$result['page_size']);
        if($result>0){
            return $result;
        }else{
            return 0;
        }
    }
} 