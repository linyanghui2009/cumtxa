<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 14-10-24
 * Time: 下午10:49
 */

class managerMode extends Data{
    /** @var  SqlDB */
    protected $db;
    protected $page=1;
    protected $page_size=5;
    protected function __construct(){
        $this->db=SqlDB::init();
    }

    function getList($rows=20,$offset=0){
        $rows=(int)$rows;
        $offset=(int)$offset;
        $sql="select * from user_admin limit {$offset},{$rows}";
        $data=$this->db->getAll($sql);
        return $data;
    }

    function changePage(){
        if(!empty($_GET['page'])){
            $page=(int)$_GET['page'];
        }else{
            $page=$this->page;
        }
        $result['page_begin']=$this->page;
        $result['page_size']=$this->page_size;
        $result['page']=$page;
        $result['manager_begin']=($result['page']-1)*$result['page_size'];
        $result['manager_total']=$this->db->getValue("select count(*) from user_admin where 1=1");
        $result['page_total']=ceil( $result['manager_total']/$result['page_size']);
        if($result>0){
            return $result;
        }else{
            return 0;
        }
    }
} 