<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 14-9-25
 * Time: 下午2:23
 */
class adminMode extends Data {
    protected function onStart(){
        parent::onStart();
        SimpleSession::init();
    }

    function login($user,$password){
        $db=SqlDB::init();
        $password=getPassWord($user,$password);
        $user=$db->quote($user);
        $password=$db->quote($password);
        $sql="select id,username,password from user_admin where username=$user and password=$password";
        $result= $db->getOne($sql);
//        var_dump($result);
        if(!empty($result)){
            $_SESSION['admin_id']=$result['id'];
            $_SESSION['admin_login']=true;
            $_SESSION['admin_username']=$result['username'];
            $_SESSION['admin_pass']=$result['password'];
        }
        return !empty($result);
    }
    function checkLogin(){
        return isset($_SESSION['admin_login'])&&$_SESSION['admin_login'];
    }

    function getName(){
        return $this->checkLogin()?$_SESSION['admin_username']:null;
    }

    function confirmLogin($pass){
        if($_SESSION['admin_pass']==getPassWord($_SESSION['admin_username'],$pass))
        return true;
    }
} 