<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 14-10-21
 * Time: 下午12:46
 */

class hynewsMode extends Data {
    /** @var  SqlDB */
    protected $db;
    protected function __construct(){
        $this->db=SqlDB::init();
    }
    function getList($rows=20,$offset=0){
        $rows=(int)$rows;
        $offset=(int)$offset;
        $sql="select * from industry_hot_news  order by `create_time` desc limit {$offset},{$rows}";
        $data=$this->db->getAll($sql);
        return $data;
    }

    function changPage(){
        if(!empty($_GET['page'])){
            $page=(int)$_GET['page'];
        }else{
//            $page=$this->$page;
            $page=1;
        }
//        $page_size=$this->$page_size;为什么输不出来？
        $result['page_begin']=1;
        $result['page_size']=15;
        $result['page']=$page;
        $result['news_begin']=($result['page']-1)*$result['page_size'];
        $result['news_total']=$this->db->getValue("select count(*) from industry_hot_news where 1=1");
        $result['page_total']=ceil( $result['news_total']/$result['page_size']);
//        var_dump($result);
        if($result>0){
            return $result;
        }else{
            return 0;
        }
    }

} 