<?php
class article extends Activity {
	protected $page = 1; //默认第几页
	protected $page_size = 15; //单页显示条数
	protected $where ="0";//0表示没有文章分类 1表示主分类 2表示次分类
	protected $title ;
	function indexTask() {//文章列表主页面  功能：标题
		$db = SqlDB::init();
		if     (isset($_GET['subtype'])&&!empty($_GET['subtype'])) {
			$subtype = $db->quote($_GET['subtype']);
			$str = "select * from news_subtype where name=$subtype";
			$result  = $db->getOne($str);
		} 
		elseif (isset($_GET['type'])&&!empty($_GET['type'])) {
			$type = $db->quote($_GET['type']);
			$str = "select * from news_type where name=$type";
			$result  = $db->getOne($str);
		} 
		else {
			$result['name']="文章";
		}
		//View::displayAsJson($result1);
		View::displayAsHtml($result,"tpl/article.php");
	}

	function articlereadTask() {//阅读文章主页面
		$db = SqlDB::init();
		if(!empty($_GET['class'])){//类别    0代表全部 1代表系内 2代表行业
			$class = (int)$_GET['class'];
		}else{
			$class = 1;
		}
		switch ($class) {
			case 1:
				$str_class = " news ";
				break;
			case 2:
				$str_class = " industry_hot_news ";
				break;
			default:
				$str_class = " news ";
				break;
		}
		if(!empty($_GET['articlename'])){
			$title = $db->quote($_GET['articlename']);
			$str = "select * from".$str_class."where title=$title";
		}else{
			if(!empty($_GET['id']))
				$id = $db->quote($_GET['id']);
			$str="select * from".$str_class."where id=$id";
		}
		$result = $db->getOne($str);
        View::displayAsHtml($result,"tpl/articleread.php");
	}

	function searchTask(){
        View::displayAsHtml(array(),"tpl/search.php");
	
	}
	//后面的全是模块 ---------------------------------------------
	//显示文章列表
	function article_selectTask() {
		$db = SqlDB::init();
		$result = array();
		if(!empty($_GET['page'])){
			$page = (int)$_GET['page'];
		}else{
			$page = $this->page;
		}
		if(!empty($_GET['class'])){//类别    0代表全部 1代表系内 2代表行业
			$class = (int)$_GET['class'];
		}else{
			$class = 1;
		}
		switch ($class) {
			case 1:
				$str_class = " news ";
				break;
			case 2:
				$str_class = " industry_hot_news ";
				break;
			default:
				$str_class = " news ";
				break;
		}
		$page_size  = $this->page_size;
		$page_begin = ($page-1)*$page_size;

        //$sql = "select id,title,date from".$str_class."where 1=1 ";
        //$countSql = "select count(*) from".$str_class."where 1=1";
        $sql = "select * from".$str_class."where 1=1 ";
        $countSql = "select count(*) from".$str_class."where 1=1";
        if(isset($_GET['type'])&&!empty($_GET['type'])) {
            $sql.=" and type=".$db->quote($_GET['type']);
            $countSql.=" and type=".$db->quote($_GET['type']);
            $result['type']=htmlspecialchars(strip_tags($_GET['type']));
        }else{
            $result['type']=null;
        }
        if(isset($_GET['subtype'])&& !empty($_GET['subtype'])){
        	if(!isset($_GET['type'])||empty($_GET['type'])){
        		$result['subtype']=null;
        	}
            $sql.=" and subtype=".$db->quote($_GET['subtype']);
            $countSql.=" and subtype=".$db->quote($_GET['subtype']);
            $result['subtype']=htmlspecialchars(strip_tags($_GET['subtype']));
        }else{
            $result['subtype']=null;
        }
        $sql.="order by date DESC LIMIT $page_begin,$page_size";
        $result['list']=$db->getAll($sql);
        $result['article_total']=$db->getValue($countSql);
		$result['page'] = $page;
		$result['class'] = $class;
		$page_total=$result['article_total']/$page_size ;
		$result['page_total'] = ceil($page_total);
        View::displayAsHtml($result,"tpl/article_part/article_select.php");
	}

	function article_searchTask() {//文章搜索
		$db = SqlDB::init();
		$result = array();
		if(!empty($_GET['page'])){
			$page = (int)$_GET['page'];
		}else{
			$page = $this->page;
		}
		$page_size = $this->page_size;
		$page_begin = ($page-1)*$page_size ;
		//分页显示的页面数
		$sql = "select id,title,date from news where 1=1 ";
        $countSql = "select count(*) from news where 1=1";
        if(isset($_GET['search'])&&!empty($_GET['search'])) {
        	$search=htmlspecialchars(strip_tags($_GET['search']));
            $sql.=" and content like '%".$search."%'";
            $countSql.=" and content like '%".$search."%'";
        }
        $sql.="order by date DESC LIMIT $page_begin,$page_size";
		$result['list']=$db->getAll($sql);
        $result['article_total']=$db->getValue($countSql);
		$result['page'] = $page;
		$page_total=$result['article_total']/$page_size ;
		$result['page_total'] = ceil($page_total);
        View::displayAsHtml($result,"tpl/article_part/article_search.php");
	}

	//阅读单个文章！！！
	function article_readTask(){
		$db = SqlDB::init();
		if(!empty($_GET['class'])){//类别    0代表全部 1代表系内 2代表行业
			$class = (int)$_GET['class'];
		}else{
			$class = 1;
		}
		switch ($class) {
			case 1:
				$str_class = " news ";
				break;
			case 2:
				$str_class = " industry_hot_news ";
				break;
			default:
				$str_class = " news ";
				break;
		}
		if(!empty($_GET['articlename'])){
			$title = $db->quote($_GET['articlename']);
			$str = "select * from".$str_class."where title=$title";
			$str_view = " and title=$title";
		}else{
			if(!empty($_GET['id']))
				$id = (int)$_GET['id'];
			$str="select * from".$str_class."where id=$id";
			$str_view =" and id=$id";
		}
		
		if(!$result=$db->getOne($str)){
			exit("找不到此文章");
		}else{
			$id1=$result['id']-1;
			$id2=$result['id']+1;
			$str1 = "select id from".$str_class."where id=$id1";
			$str2 = "select id from".$str_class."where id=$id2";
			$result['id1'] = $db->getExist($str1);
			$result['id2'] = $db->getExist($str2);

			$result['class'] = $class;

			$views = $result['views']+1;
			$str_view = "update".$str_class."set views=$views where 1=1".$str_view;
			$db->sqlExec($str_view);
		}
        View::displayAsHtml($result,"tpl/article_part/article_read.php");
	}

	//用于首页展示有限个文章
	function article_show_xineiTask() {
		$db = SqlDB::init();
		$str = "select * from news where type='学生培养' or type='首页专区' order by date DESC limit 12";
		$result = $db->getAll($str);
		$result['class']=1;
		//print_r($result1);
        View::displayAsHtml($result,"tpl/article_part/article_show.php");
	}

	function article_show_hangyeTask() {
		$db = SqlDB::init();
		$str = "select * from industry_hot_news order by date DESC limit 12";
		$result = $db->getAll($str);
		$result['class']=2;
		//print_r($result1);
        View::displayAsHtml($result,"tpl/article_part/article_show.php");
	}

	function article_hotTask(){
		$db = SqlDB::init();
		$str = "select id,title from industry_hot_news order by views DESC,date DESC limit 12";
		$result = $db->getAll($str);
        View::displayAsHtml($result,"tpl/article_part/article_hot.php");
	}

}
