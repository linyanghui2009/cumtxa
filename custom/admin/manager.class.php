<?php

import("custom.data.adminMode");
import("custom.data.managerMode");
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 14-10-23
 * Time: 下午10:16
 */

class manager extends Activity{
    /** @var CmsView  */
    protected $cms;
    /** @var  adminMode */
    protected $user;
    /** @var  managerMode */
    protected $manager;
    protected function __construct() {
        $this->cms=CmsView::init();
        $this->cms->setPageTitle("管理者");
        $this->cms->setControlFile("tpl/admin/manager/control.json");
        $this->user=adminMode::init();
        $this->manager=managerMode::init();
        $this->checkLogin();
        $name=$this->user->getName();
        $this->cms->setUserName($name);
    }

    function checkLogin(){
        if(!$this->user->checkLogin()){
            $webRouter=WebRouter::init();
            header("Location:".$webRouter->getPage("user","login"));
            // exit();
        }

    }

    function tableTask(){
        $this->cms->setActionTitle("管理者查看");
        $result=$this->manager->changePage();
        $result['list']=$this->manager->getList($result['page_size'],$result['manager_begin']);
        $this->cms->tableScene($result,"tpl/admin/manager/table.php");
    }

    function createTask(){
        $this->cms->normalScene(array(),"tpl/admin/manager/create.php",
            CmsView::TYPE_FORM| CmsView::TYPE_JQUERY| CmsView::TYPE_EDITOR);
    }

    function createSubmitTask(){
        $db=SqlDB::init();
        if($this->user->confirmLogin($_POST['message'])){
            $data['username']=$_POST['manager_name'];
            $data['password']=getPassWord($data['username'],$_POST['manager_password']);
            $result=$db->insert("user_admin",$data);
            if($result>0){
                $webRouter=WebRouter::init();
                header("Location:".$webRouter->getPage("manager","table"));
            }else{
                echo "fail";
            }
        }else{
            echo "<meta charset='utf-8'/><script>alert('您不具有添加管理者权限！');window.history.back();</script>";
            return ;
        }

    }

    function deleteTask(){
        $db=SqlDB::init();
        $db->delete("user_admin",$_GET['id']);
        $webRouter=WebRouter::init();
        header("Location:".$webRouter->getPage("manager","table"));
    }

    function modifyTask(){
        $this->cms->setActionTitle("修改密码");
        $this->cms->normalScene(array(),"tpl/admin/manager/modify.php",
            CmsView::TYPE_FORM| CmsView::TYPE_JQUERY| CmsView::TYPE_EDITOR);
    }

    function modifySubmitTask(){
        $db=SqlDB::init();
        if($this->user->confirmLogin($_POST['old_password'])&&$_POST['new_password']==$_POST['renew_password']){
            $id=$_POST['admin_id'];
            $data['username']=$this->user->getName();
            $data['password']=getPassWord($data['username'],$_POST['new_password']);
            $result=$db->update("user_admin",$id,$data);
        if($result>0){
            $webRouter=WebRouter::init();
            header("Location:".$webRouter->getPage("manager","table"));
        }else{
            echo "fail";
        }
        }else{
            echo "<meta charset='utf-8'/><script>alert('您不具有修改该管理员密码权限！');window.history.back();</script>";
            return ;
        }
    }

} 