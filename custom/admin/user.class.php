<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 14-9-25
 * Time: 下午2:16
 */
import("custom.data.adminMode");
class user extends Activity{
    /** @var CmsView  */
    protected $cms;
    /** @var  adminMode */
    protected $user;
    protected function __construct() {
        parent::__construct();
        $this->cms=CmsView::init();
        $this->user=adminMode::init();
    }
    function loginTask(){
        $web=WebRouter::init();
        $this->cms->loginScene($web->getAction("loginSubmit"));
    }

    function loginSubmitTask(){
        if(!isset($_POST['user'])||empty($_POST['user'])||!isset($_POST['pass'])){
            echo "<meta charset='utf-8'/><script>alert('信息不完整！');window.history.back();</script>";
            return ;
        }
        $result=$this->user->login($_POST['user'],$_POST['pass']);
        if(!$result){
            echo "<meta charset='utf-8'/><script>alert('您的用户名或密码错误！');window.history.back();</script>";
        }else{
            $webRouter=WebRouter::init();
            header("Location:".$webRouter->getPage("news","table"));
        }

    }

    function loginoutTask(){
        $webRouter=WebRouter::init();
        header("Location:".$webRouter->getPage("user","login"));
    }

    function passwordTask(){
        $webRouter=WebRouter::init();
        header("Location:".$webRouter->getPage("manager","modify"));
    }
} 