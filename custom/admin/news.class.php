<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 14-9-25
 * Time: 下午4:23
 */
import("custom.data.adminMode");
import("custom.data.newsMode");
class news extends Activity {
    /** @var CmsView  */
    protected $cms;
    /** @var  adminMode */
    protected $user;
    /** @var  newsMode */
    protected $news;

    protected function __construct() {
        $this->cms=CmsView::init();
        $this->cms->setPageTitle("新闻管理");
        $this->cms->setControlFile("tpl/admin/news/control.json");
        $this->user=adminMode::init();
        $this->news=newsMode::init();
        $this->checkLogin();
        $name=$this->user->getName();
        $this->cms->setUserName($name);
    }
    //检查是否登陆
    function checkLogin(){
        if(!$this->user->checkLogin()){
            $webRouter=WebRouter::init();
            header("Location:".$webRouter->getPage("user","login"));
           // exit();
        }

    }
    //显示新闻
    function tableTask(){
        $this->cms->setActionTitle("新闻查看");
        $result=$this->news->changePage();
        $result['list']=$this->news->getList($result['page_size'],$result['news_begin']);
        $this->cms->tableScene($result,"tpl/admin/news/table.php");
    }
    //创建新闻
    function createTask(){
        $results['subtype']=$this->news->subtypeNews();
        $this->cms->normalScene($results,"tpl/admin/news/create.php",
            CmsView::TYPE_FORM| CmsView::TYPE_JQUERY| CmsView::TYPE_EDITOR);

    }
    //提交创建的新闻并插入到数据库中
    function createSubmitTask(){
        $db=SqlDB::init();
   //     var_dump($_POST['type']);
        $id=$db->quote($_POST['type']);
        $data['type']=$db->getValue("select `pretype` from `news_subtype` where id=$id");
        $data['subtype']=$db->getValue("select `name`  from `news_subtype` where id=$id");
        $data['title']=$_POST['title'];
        $data['content']=$_POST['contentInput'];
        $data['editer']=$_POST['editer'];
        $data['date']=$_POST['date'];
        $result=$db->insert("news",$data);
        if($result>0){
            $webRouter=WebRouter::init();
            header("Location:".$webRouter->getPage("news","table"));
        }else{
            echo "fail";
        }
    }
    //删除一条新闻
    function deleteTask(){
        $db=SqlDB::init();
        $db->delete("news",$_GET['id']);
        $webRouter=WebRouter::init();
        header("Location:".$webRouter->getPage("news","table"));
    }
    //修改新闻显示之前的信息
    function modifyTask(){
        $db=SqlDB::init();
        $id=$db->quote($_GET['id']);
        $result['detail']=$db->getOne("select * from news where id=$id");
        $result['subtype']=$db->getAll("select `id`,`name`,`pretype` from `news_subtype`");
        $this->cms->normalScene($result,"tpl/admin/news/modify.php",
            CmsView::TYPE_FORM| CmsView::TYPE_JQUERY| CmsView::TYPE_EDITOR);

    }
    //保存修改并更新到数据库中
    function  savemodifyTask(){
        $db=SqlDB::init();
        $type_id=$db->quote($_POST['type']);
        $data['type']=$db->getValue("select `pretype` from `news_subtype` where id=$type_id");
        $data['subtype']=$db->getValue("select `name`  from `news_subtype` where id=$type_id");
        $id=$_POST['news_id'];
        $data['title']=$_POST['title'];
        $data['content']=$_POST['contentInput'];
        $data['editer']=$_POST['editer'];
        $data['date']=$_POST['date'];
        $result=$db->update("news",$id,$data);
        if($result>0){
            $webRouter=WebRouter::init();
            header("Location:".$webRouter->getPage("news","table"));
        }else{
            echo "<meta charset='utf-8'/><script>alert('信息没有被修改！');window.history.back();</script>";
        }

    }
} 