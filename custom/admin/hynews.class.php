<?php
import("custom.data.adminMode");
import("custom.data.hynewsMode");
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 14-9-26
 * Time: 下午10:18
 */

class hynews extends Activity{
    /** @var CmsView  */
    protected $cms;
    /** @var  adminMode */
    protected $user;
    /** @var  hynewsMode */
    protected $hynews;
    protected function __construct() {
        $this->cms=CmsView::init();
        $this->cms->setPageTitle("新闻管理");
        $this->cms->setControlFile("tpl/admin/hynews/control.json");
        $this->user=adminMode::init();
        $this->hynews=hynewsMode::init();
      //  $this->checkLogin();
        $name=$this->user->getName();
        $this->cms->setUserName($name);

    function checkLogin(){
        if(!$this->user->checkLogin()){
             $webRouter=WebRouter::init();
             header("Location:".$webRouter->getPage("user","login"));
            // exit();
        }

    }
}
    function tableTask(){
        $this->cms->setActionTitle("行业新闻查看");
        $result=$this->hynews->changPage();
        $result['list']=$this->hynews->getList($result['page_size'],$result['news_begin']);
        $this->cms->tableScene($result,"tpl/admin/hynews/table.php");

    }

    function createTask(){
            $this->cms->normalScene(array(),"tpl/admin/hynews/create.php",
                CmsView::TYPE_FORM| CmsView::TYPE_JQUERY| CmsView::TYPE_EDITOR);
    }

    function createSubmitTask(){
        $db=SqlDB::init();
        $data['title']=$_POST['title'];
        $data['content']=$_POST['contentInput'];
        $data['editer']=$_POST['editer'];
        $data['date']=$_POST['date'];
        $result=$db->insert("industry_hot_news",$data);
        if($result>0){
            $webRouter=WebRouter::init();
            header("Location:".$webRouter->getPage("hynews","table"));
        }else{
            echo "fail";
        }
    }

    function deleteTask(){
        $db=SqlDB::init();
        $db->delete("industry_hot_news",$_GET['id']);
        $webRouter=WebRouter::init();
        header("Location:".$webRouter->getPage("hynews","table"));
    }

    function modifyTask(){
        $db=SqlDB::init();
        $id=$db->quote($_GET['id']);
        $result['detail']=$db->getOne("select * from industry_hot_news where id=$id");
        $this->cms->normalScene($result,"tpl/admin/hynews/modify.php",
        CmsView::TYPE_FORM| CmsView::TYPE_JQUERY| CmsView::TYPE_EDITOR);
    }

    function  savemodifyTask(){
        $db=SqlDB::init();
        $id=$_POST['id'];
        $data['title']=$_POST['title'];
        $data['content']=$_POST['contentInput'];
        $data['editer']=$_POST['editer'];
        $data['date']=$_POST['date'];
        $result=$db->update("industry_hot_news",$id,$data);
        if($result>0){
            $webRouter=WebRouter::init();
            header("Location:".$webRouter->getPage("hynews","table"));
        }else{
            echo "<meta charset='utf-8'/><script>alert('信息没有被修改！');window.history.back();</script>";
        }
    }
}