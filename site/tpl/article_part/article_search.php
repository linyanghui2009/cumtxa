
<!-- 第一部分 显示文章位置导航-->
	<div class="article_where">
		<a href="/cumtxa/article">文章 </a>
		> 搜索 ></a>
	</div>
<!-- 第二部分 显示文章信息和内容 -->
	<div class="article_main">
		<ul>
			<?php foreach($result['list'] as $value) { ?>
			<li>
				<span class="article_title">
					<a href="<?php e_page("article","articleread","id={$value['id']}");?>">
				        <?php echo $value['title'];?></a>
				</span>
				<span class="article_date">
					<?php echo $value['date'];?>
				</span>
			</li>
			<?php } ?>
		</ul>
	</div>
<!-- 第三部分 显示分页 -->
	<div class="article_page">
		<a href="<?php e_page("article","type",array("type"=>$result['type'],"subtype"=>$result['subtype'],"page"=>1));?>">
            首页</a>
		<?php
		$total = $result['page_total'];//页面总数
		$page = $result['page'];//当前页面
		$page_start = floor($page/10)*10+1;//(floor($page/10))*10+1;//当前页面所处的分段，也是当前分段的开头页面
		$page_end = (ceil($page/10))*10;
		
		if($page!=1){ ?>
			<a href="<?php e_page("article","index",array("type"=>$result['type'],"subtype"=>$result['subtype'],"page"=>$page-1));?>"> 上一页 </a>
		<?php } ?>
		
		<?php if($page_start>10){ ?>
			<a href="<?php e_page("article","index",array("type"=>$result['type'],"subtype"=>$result['subtype'],"page"=>$page_start-10));?>"> ... </a>
		<?php } ?>

		<?php for($i=$page_start;($i<=$total)&&($i<=$page_end);$i++){//页面数字显示 ?>
			<a href="<?php e_page("article","index",array("type"=>$result['type'],"subtype"=>$result['subtype'],"page"=>$i));?>">
			 <?php echo $i==$page?"[{$i}]":$i;?> </a>
		<?php } ?>

		<?php if($page_end<$total){ ?>
			<a href="<?php e_page("article","index",array("type"=>$result['type'],"subtype"=>$result['subtype'],"page"=>$page_start+10));?>"> ... </a>
		<?php } ?>
		
		<?php if($page!=$total){ ?>
		<a href="<?php e_page("article","index",array("type"=>$result['type'],"subtype"=>$result['subtype'],"page"=>$page+1));?>">
             下一页 </a>
		<?php } ?>
		<a href="<?php e_page("article","index",array("type"=>$result['type'],"subtype"=>$result['subtype'],"page"=>$total));?>">
             尾页 </a>
	</div>