<ul class="article_title">
<?php foreach ($result as $value) { ?>
	<li>
		<span class="title">
            <a href="<?php e_page("article","articleread",array("id"=>$value['id'],"class"=>$result['class']));?>">
                <?php
                echo mb_substr($value['title'],0,20,"utf-8");
                if (strlen($value['title'])>20) {
                	echo "...";
                }
                ?>
            </a>
		</span>
        <span class="time"><time><?php echo $value['date'];?></time></span>
	</li>
<?php } ?>
</ul>