<!-- 第一部分 显示文章位置导航-->
	<div class="article_where">
		<a href="<?php e_page("article","index","class={$result['class']}");?>"><?php if($result['class']==1){echo "系内新闻公告";}else{echo "行业新闻";}?></a>
		>

		<?php if(!empty($result['type'])){?>
		<a href="<?php e_page("article","index",array("type"=>$result['type'],"class"=>$result['class']));?>">
            <?php echo $result['type'];?></a>
		>
		<?php } ?>

		<?php if(!empty($result['subtype'])){?>
		<a href="<?php e_page("article","index",array("subtype"=>$result['subtype'],"class"=>$result['class']));?>">
            <?php echo $result['subtype'];?></a>
		<?php } ?>
	</div>
<!-- 第二部分 显示文章信息和内容 -->
	<div class="article_main">
		<ul>
			<?php foreach($result['list'] as $value) { ?>
			<li>
				<span class="article_title">
					<a href="<?php e_page("article","articleread",array("id"=>$value['id'],"class"=>$result['class']));?>">
				        <?php
                		echo mb_substr($value['title'],0,32,"utf-8");
                		if (mb_strlen($value['title'],"utf-8")>32)
                			echo "...";
                		?>
                	</a>
				</span>
				<span class="article_date">
					<?php echo $value['date'];?>
				</span>
			</li>
			<?php } ?>
		</ul>
	</div>
<!-- 第三部分 显示分页 -->
	<div class="article_page">
		<a href="<?php e_page("article","index",array("type"=>$result['type'],"subtype"=>$result['subtype'],"page"=>1,"class"=>$result['class']));?>">
        	<span class="page_first">首页</span></a>
		<?php
		$total = $result['page_total'];//页面总数
		$page = $result['page'];//当前页面
		$page_start = floor($page/10)*10+1;//(floor($page/10))*10+1;//当前页面所处的分段，也是当前分段的开头页面
		$page_end = (ceil($page/10))*10;
		
		if($page!=1){ ?>
			<a href="<?php e_page("article","index",array("type"=>$result['type'],"subtype"=>$result['subtype'],"page"=>$page-1,"class"=>$result['class']));?>"><span class="page_before">上一页</span></a>
		<?php }else{echo "<span class='page_before'>上一页</span>";} ?>
		
		<?php if($page_start>10){ ?>
			<a href="<?php e_page("article","index",array("type"=>$result['type'],"subtype"=>$result['subtype'],"page"=>$page_start-10,"class"=>$result['class']));?>"><span class="page_part1">...</span></a>
		<?php } ?>

		<?php for($i=$page_start;($i<=$total)&&($i<=$page_end);$i++){//页面数字显示 ?>
			<a href="<?php e_page("article","index",array("type"=>$result['type'],"subtype"=>$result['subtype'],"page"=>$i,"class"=>$result['class']));?>">
				<span class="page_num"><?php echo $i==$page?"[{$i}]":$i;?></span></a>
		<?php } ?>

		<?php if($page_end<$total){ ?>
			<a href="<?php e_page("article","index",array("type"=>$result['type'],"subtype"=>$result['subtype'],"page"=>$page_start+10,"class"=>$result['class']));?>"><span class="page_part2">...</span></a>
		<?php } ?>
		
		<?php if($page!=$total){ ?>
		<a href="<?php e_page("article","index",array("type"=>$result['type'],"subtype"=>$result['subtype'],"page"=>$page+1,"class"=>$result['class']));?>">
            <span class="page_next">下一页</span></a>
		<?php }else{echo "<span class='page_next'>下一页</span>";} ?>
		<a href="<?php e_page("article","index",array("type"=>$result['type'],"subtype"=>$result['subtype'],"page"=>$total,"class"=>$result['class']));?>">
            <span class="page_last">尾页</span></a>
	</div>