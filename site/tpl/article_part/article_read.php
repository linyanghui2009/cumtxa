<?php
$id1=$result['id1'];
$id2=$result['id2'];
?>

<div class="article_title">
	<?php echo $result['title'];?>
</div>

<div class="article_info">
	<span>日期：<?php echo $result['date'];?></span>
	<span>编辑人：<?php echo $result['editer'];?></span>
	<span>浏览次数：<?php echo $result['views'];?></span>
</div>

<div class="article_view">
	<?php echo nl2br($result['content']);?>
</div>

<div class="article_page">
	<?php if($result['id1']==true){ $id=$result['id']-1; ?>
	<a href="<?php e_page("article","articleread",array("id"=>$id,"class"=>$result['class']));?>
	"><span class="read_before">上一篇</span></a>
	<?php }else{echo "<span class='read_before'>已经是第一篇</span>";} 
	if($result['id2']==true){ $id=$result['id']+1; ?>
	<a href="<?php e_page("article","articleread",array("id"=>$id,"class"=>$result['class']));?>
	"><span class="read_next">下一篇</span></a>
	<?php  }else{echo "<span class='read_next'>已经是最后一篇</span>";} ?>
</div>
