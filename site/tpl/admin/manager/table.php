<a href="<?php e_page("manager", "table"); ?>"></a>
<table id="dataTable">
    <thead>
    <tr>
        <th>id</th>
        <th>管理者用户名</th>
        <th>操作</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th>id</th>
        <th>管理者用户名</th>
        <th>操作</th>
    </tr>
    </tfoot>
    <tbody>
    <?php foreach ($result['list'] as $v){ ?>
        <tr>
            <td><?php echo $v['id'];?></td>
            <td><?php echo $v['username'];?></td>
            <td>
            <?php if($v['username']!='csdn'){?>
                <a href='<?php e_action("delete","id={$v['id']}");?>'>删除</a>
            <?php } ?>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>
<div id="dataPage">
    <ul>
        <li>共<span><?php echo $result['page_total'];?></span>页/<span><?php echo $result['manager_total'];?></span>条记录</li>
        <li><a href="<?php e_page("manager","table",array("page"=>$result['page_begin']));?>">首页</a> </li>
        <?php if($result['page']!=1){ ?>
            <li><a href="<?php e_page("news","table",array("page"=>$result['page']-1));?>">上一页</a></li>
        <?php } ?>

        <?php if($result['page_total']==1){?>
            <li class="page"><a href='<?php e_page("manager","table","page=1");?>'>1</a></li>
        <?php } ?>
        <?php if($result['page_total']==2){?>
            <li class="page"><a href='<?php e_page("manager","table","page=1");?>'>1</a></li>
            <li class="page"><a href="<?php e_page("manager","table","page=2");?>">2</a></li>
        <?php } ?>
        <?php if($result['page_total']>=3){?>
            <li class="page"><a href='<?php e_page("manager","table","page=1");?>'>1</a></li>
            <li class="page"><a href="<?php e_page("manager","table","page=2");?>">2</a></li>
            <li class="page"><a href="<?php e_page("manager","table","page=3");?>">3</a></li>
        <?php } ?>

        <?php if($result['page']!=$result['page_total']&&$result['page']<$result['page_total']){ ?>
            <li><a href="<?php e_page("manager","table",array("page"=>$result['page']+1));?>">下一页</a></li>
        <?php } ?>
        <li><a href="<?php e_page("manager","table",array("page"=>$result['page_total']));?>">尾页</a> </li>
    </ul>
</div>