<form action="<?php e_page("manager","modifySubmit");?>" method="post">
    <fieldset>
        <legend>修改密码</legend>
        <input type="hidden" name="admin_id" value="<?php echo $_SESSION['admin_id']?>"/>
        <label for="old_password">*当前密码</label>
        <input type="text" name="old_password" id="old_password" placeholder="请输入当前密码"/>
        <br/>
        <label for="new_password">*新密码</label>
        <input type="text" name="new_password" id="new_password" placeholder="请输入新密码"/>
        <br/>
        <label for="renew_password">*确认新密码</label>
        <input type="text" name="renew_password" id="renew_password" placeholder="请再次输入新密码"/>
        <br/>
        <br/>
    </fieldset>
    <input type="submit"  value="保存修改" />
    <input type="reset" value="重置修改"/>
</form>
