<form action="<?php e_page("hynews","savemodify");?>" method="post">
    <fieldset>
        <legend>新闻信息</legend>
        <input type="hidden" name="id" value="<?php echo $result['detail']['id']?>"/>

        <label for="title">新闻标题</label>
        <input type="text" name="title" id="title" value="<?php echo $result['detail']['title'];?>" placeholder="请输入新闻标题"/>
        <br/>
        <label for="editer">编辑人</label>
        <input type="text" name="editer" id="editer" value="<?php echo $result['detail']['editer'];?>" placeholder="请输入编辑人"/>
        <br/>
        <label for="date">编辑时间</label>
        <input type="text" name="date" id="date" value="<?php echo $result['detail']['date'];?>" placeholder="请输入编辑时间"/>
        <br/>
    </fieldset>
    <fieldset>
        <legend>新闻内容</legend>
        <script id="contentInput" name="contentInput" class="editor" type="text/plain"><?php echo $result['detail']['content'];?></script>
    </fieldset>
    <input type="submit"  value="保存修改" />
</form>