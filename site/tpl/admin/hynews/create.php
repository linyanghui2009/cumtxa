<form action="<?php e_page("hynews","createSubmit");?>" method="post">
    <fieldset>
        <legend>新闻信息</legend>
        <label for="title">新闻标题</label>
        <input type="text" name="title" id="title" placeholder="请输入新闻标题"/>
        <br/>
        <label for="picture">编辑人</label>
        <input type="text" name="editer" id="editer" placeholder="请输入编辑人"/>
        <br/>
        <label for="content">编辑时间</label>
        <input type="text" name="date" id="date" placeholder="请输入编辑时间"/>
        <br/>
    </fieldset>
    <fieldset>
        <legend>新闻内容</legend>
        <script id="contentInput" name="contentInput" class="editor" type="text/plain"></script>
    </fieldset>
    <input type="submit"  value="添加" />
    <input type="reset" value="重置"/>
</form>
