<form action="<?php e_page("news","createSubmit");?>" method="post">
    <fieldset>
        <legend>新闻信息</legend>
        <label for="type">新闻类型</label>
        <select name="type" id="type">
            <option value="0">所有</option>
            <?php foreach($result['subtype'] as $v ){?>
             <option value="<?php echo $v['id'] ?>"><?php echo $v['pretype']?>-<?php echo $v['name']?></option>
            <?php }?>
        </select>
        <br/>
        <label for="title">新闻标题</label>
        <input type="text" name="title" id="title" placeholder="请输入新闻标题"/>
        <br/>
        <label for="editer">编辑人</label>
        <input type="text" name="editer" id="editer" placeholder="请输入编辑人"/>
        <br/>
        <label for="date">编辑时间</label>
        <input type="text" name="date" id="date" placeholder="请输入编辑时间"/>
        <br/>
    </fieldset>
    <fieldset>
        <legend>新闻内容</legend>
        <script id="contentInput" name="contentInput" class="editor" type="text/plain"></script>
    </fieldset>
    <input type="submit"  value="添加" />
    <input type="reset" value="重置"/>
</form>
