<a href="<?php e_page("news", "create"); ?>"></a>
<table id="dataTable">
    <thead>
    <tr>
        <th>id</th>
        <th>新闻类型</th>
        <th>次级分类</th>
        <th>新闻标题</th>
        <th>编辑人</th>
        <th>编辑时间</th>
        <th>操作</th>
        <th>操作</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th>id</th>
        <th>新闻类型</th>
        <th>次级分类</th>
        <th>新闻标题</th>
        <th>编辑人</th>
        <th>编辑时间</th>
        <th>操作</th>
        <th>操作</th>
    </tr>
    </tfoot>
    <tbody>
    <?php foreach ($result['list'] as $v){ ?>
        <tr>
            <td><?php echo $v['id'];?></td>
            <td><?php echo $v['type'];?></td>
            <td><?php echo $v['subtype'];?></td>
            <td><?php echo $v['title'];?></td>
            <td><?php echo $v['editer'];?></td>
            <td><?php echo $v['date'];?></td>
            <td><a href='<?php e_action("modify","id={$v['id']}");?>'>修改</a></td>
            <td><a href='<?php e_action("delete","id={$v['id']}");?>'>删除</a></td>
        </tr>
    <?php } ?>
    </tbody>
</table>
<div id="dataPage">
    <ul>
        <li>共<span><?php echo $result['page_total'];?></span>页/<span><?php echo $result['news_total'];?></span>条记录</li>
        <li><a href="<?php e_page("news","table",array("page"=>$result['page_begin']));?>">首页</a> </li>
        <?php if($result['page']!=1){ ?>
            <li><a href="<?php e_page("news","table",array("page"=>$result['page']-1));?>">上一页</a></li>
        <?php } ?>

        <?php if($result['page_total']==1){?>
            <li class="page"><a href='<?php e_page("news","table",array("page"=>$result['page_total']));?>'><?php echo $result['page_total']?></a></li>
        <?php } ;?>
        <?php if($result['page_total']==2){?>
            <li class="page"><a href='<?php e_page("news","table",array("page"=>$result['page_total']-1));?>'><?php echo $result['page_total']-1?></a></li>
            <li class="page"><a href='<?php e_page("news","table",array("page"=>$result['page_total']));?>'><?php echo $result['page_total']?></a></li>
        <?php }; ?>
        <?php if($result['page_total']==3){?>
            <li class="page"><a href='<?php e_page("news","table",array("page"=>$result['page_total']-2));?>'><?php echo $result['page_total']-2?></a></li>
            <li class="page"><a href='<?php e_page("news","table",array("page"=>$result['page_total']-1));?>'><?php echo $result['page_total']-1?></a></li>
            <li class="page"><a href='<?php e_page("news","table",array("page"=>$result['page_total']));?>'><?php echo $result['page_total']?></a></li>
        <?php }; ?>
        <?php if($result['page_total']==4){?>
            <li class="page"><a href='<?php e_page("news","table",array("page"=>$result['page_total']-3));?>'><?php echo $result['page_total']-3?></a></li>
            <li class="page"><a href='<?php e_page("news","table",array("page"=>$result['page_total']-2));?>'><?php echo $result['page_total']-2?></a></li>
            <li class="page"><a href='<?php e_page("news","table",array("page"=>$result['page_total']-1));?>'><?php echo $result['page_total']-1?></a></li>
            <li class="page"><a href='<?php e_page("news","table",array("page"=>$result['page_total']));?>'><?php echo $result['page_total']?></a></li>
        <?php }; ?>
        <?php if($result['page_total']>=5){?>
            <?php if($result['page']==1){?>
                <li class="page"><a href='<?php e_page("news","table",array("page"=>$result['page']));?>'><?php echo $result['page']?></a></li>
                <li class="page"><a href='<?php e_page("news","table",array("page"=>$result['page']+1));?>'><?php echo $result['page']+1?></a></li>
                <li class="page"><a href='<?php e_page("news","table",array("page"=>$result['page']+2));?>'><?php echo $result['page']+2?></a></li>
            <?php } ?>
            <?php if($result['page']==2){?>
                <li class="page"><a href='<?php e_page("news","table",array("page"=>$result['page']-1));?>'><?php echo $result['page']-1?></a></li>
                <li class="page"><a href='<?php e_page("news","table",array("page"=>$result['page']));?>'><?php echo $result['page']?></a></li>
                <li class="page"><a href='<?php e_page("news","table",array("page"=>$result['page']+1));?>'><?php echo $result['page']+1?></a></li>
                <li class="page"><a href='<?php e_page("news","table",array("page"=>$result['page']+2));?>'><?php echo $result['page']+2?></a></li>
            <?php } ?>
            <?php if(($result['page']>=3)&&($result['page']<=$result['page_total']-2)){ ?>
                <li class="page"><a href='<?php e_page("news","table",array("page"=>$result['page']-2));?>'><?php echo $result['page']-2?></a></li>
                <li class="page"><a href='<?php e_page("news","table",array("page"=>$result['page']-1));?>'><?php echo $result['page']-1?></a></li>
                <li class="page"><a href='<?php e_page("news","table",array("page"=>$result['page']));?>'><?php echo $result['page']?></a></li>
                <li class="page"><a href='<?php e_page("news","table",array("page"=>$result['page']+1));?>'><?php echo $result['page']+1?></a></li>
                <li class="page"><a href='<?php e_page("news","table",array("page"=>$result['page']+2));?>'><?php echo $result['page']+2?></a></li>
            <?php } ?>
            <?php if($result['page']==$result['page_total']-1){?>
                <li class="page"><a href='<?php e_page("news","table",array("page"=>$result['page']-2));?>'><?php echo $result['page']-2?></a></li>
                <li class="page"><a href='<?php e_page("news","table",array("page"=>$result['page']-1));?>'><?php echo $result['page']-1?></a></li>
                <li class="page"><a href='<?php e_page("news","table",array("page"=>$result['page']));?>'><?php echo $result['page']?></a></li>
                <li class="page"><a href='<?php e_page("news","table",array("page"=>$result['page']+1));?>'><?php echo $result['page']+1?></a></li>
            <?php } ?>
            <?php if($result['page']==$result['page_total']){?>
                <li class="page"><a href='<?php e_page("news","table",array("page"=>$result['page']-2));?>'><?php echo $result['page']-2?></a></li>
                <li class="page"><a href='<?php e_page("news","table",array("page"=>$result['page']-1));?>'><?php echo $result['page']-1?></a></li>
                <li class="page"><a href='<?php e_page("news","table",array("page"=>$result['page']));?>'><?php echo $result['page']?></a></li>
            <?php } ?>
        <?php }; ?>

        <?php if($result['page']!=$result['page_total']&&$result['page']<$result['page_total']){ ?>
            <li><a href="<?php e_page("news","table",array("page"=>$result['page']+1));?>">下一页</a></li>
        <?php } ?>
        <li><a href="<?php e_page("news","table",array("page"=>$result['page_total']));?>">尾页</a> </li>
    </ul>
</div>
