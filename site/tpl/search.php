<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<base href="<?php echo $s['siteRoot'];?>tpl/" />
	<title>搜索结果-- CUMT 信息安全</title>
	<link rel="stylesheet" type="text/css" href="style/reset.css" />
	<link rel="stylesheet" type="text/css" href="style/index.css" />
	<link rel="shortcut icon" href="image/favicon.ico" /> 
	<script type="text/javascript" src="../plugin/jquery-1.10.2.min.js"></script>
</head>
<body>
	<header>
		<?php import_part("Custom.module","header"); ?>
	</header>
	<div id="container" class="wrapper">
		<div class="info">
			<div class="article_content">
				<?php import_part("Custom.article","article_search"); ?>
			</div>
			<div class="article_hot">
				<?php import_part("Custom.article","article_hot"); ?>
			</div>
		</div>
	</div>
	<footer>
		<?php import_part("Custom.module","footer"); ?>
	</footer>
</body>
</html>