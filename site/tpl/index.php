<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <base href="<?php echo $s['siteRoot'];?>tpl/" />
	<title>CUMT 信息安全</title>
    <link rel="stylesheet" type="text/css" href="style/reset.css" />
	<link rel="stylesheet" type="text/css" href="style/index.css" />
    <link rel="shortcut icon" href="image/favicon.ico" /> 
    <script type="text/javascript" src="../plugin/jquery-1.10.2.min.js"></script>
</head>
<body>
	<header>
		<?php import_part("Custom.module","header"); ?>
	</header>
    <div id="picSlider">
        <?php include("module/picslider.php");?>
    </div>

	<div id="container" class="wrapper">
        <div class="info">
            <div class="links">
                <div class="title">
                    <h2>系内新闻公告</h2>
                    <span class="more"><a class="link-more" href="<?php e_page("article","index");?>">More..</a></span>
                </div>
                <?php import_part("custom.article","article_show_xinei");?>
            </div>
            <div class="files">
                <div class="title">
                    <h2>行业新闻</h2>
                    <span class="more"><a class="link-more" href="<?php e_page("article","index","class=2");?>">More..</a></span>
                </div>
                <?php import_part("custom.article","article_show_hangye");?>
            </div>
        </div>
    </div>

	<footer>
		<?php import_part("Custom.module","footer"); ?>
	</footer>
</body>
</html>