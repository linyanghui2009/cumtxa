<div class="nav-inner">
	<ul class="nav-menu">
		<li class="menu-item item1">
			<a class="menulink" href="<?php e_page("home","index");?>">网站首页</a>
		</li>
		<li class="menu-item item2">
			<a class="menulink" href="<?php e_page("article","index",array("type"=>"网站信息","subtype"=>"专业概述"));?>">专业概述</a>
			<ul class="submenu item2">
				<li class="subitem item2-1">
					<a class="sublink" href="<?php e_page("article","articleread","articlename=专业介绍");?>">专业介绍</a>
				</li>
			</ul>
		</li>
		<li class="menu-item item3">
			<a class="menulink" href="<?php e_page("article","index","type=学生培养");?>">学生培养</a>
			<ul class="submenu item3">
				<li class="subitem item3-1">
					<a class="sublink" href="<?php e_page("article","index",array("type"=>"学生培养","subtype"=>"学科竞赛"));?>">学科竞赛</a>
				</li>
				<li class="subitem item3-2">
					<a class="sublink" href="<?php e_page("article","index",array("type"=>"学生培养","subtype"=>"学生风采"));?>">学生风采</a>
				</li>
				<li class="subitem item3-3">
					<a class="sublink" href="<?php e_page("article","index",array("type"=>"学生培养","subtype"=>"就业指南"));?>">就业指南</a>
				</li>
				<li class="subitem item3-4">
					<a class="sublink" href="<?php e_page("article","index",array("type"=>"学生培养","subtype"=>"学习规划"));?>">学习规划</a>
				</li>
			</ul>
		</li>
		<li class="menu-item item4">
			<a class="menulink" href="<?php e_page("article","index","type=学科建设");?>">学科建设</a>
			<ul class="submenu item4">
				<li class="subitem item4-1">
					<a class="sublink" href="<?php e_page("article","index",array("type"=>"学科建设","subtype"=>"培养计划"));?>">培养计划</a>
				</li>
				<li class="subitem item4-2">
					<a class="sublink" href="<?php e_page("article","index",array("type"=>"学科建设","subtype"=>"课程建设"));?>">课程建设</a>
				</li>
			</ul>
		</li>
		<li class="menu-item item5">
			<a class="menulink" href="<?php e_page("bbs","index");?>">学习论坛</a>
		</li>
		<li class="menu-item item6">
			<a class="menulink" href="<?php e_page("article","articleread","articlename=计算机学院信息安全系");?>">联系我们</a>
		</li>
</div>