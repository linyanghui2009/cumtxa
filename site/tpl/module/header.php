<div class="header-inner">
	<div class="logo">
		<a class="link-logo"   href="<?php e_page("home","index");?>"></a>
        <a class="link-logo-title-cn"   href="<?php e_page("home","index");?>"></a>
        <a class="link-logo-title-eng"   href="<?php e_page("home","index");?>">Department of Information Security</a>
	</div>
	<div class="search">
		<form action="<?php e_page("article","search");?>" method="GET">
            <label for="searchInput"></label>
			<input type="text" id="searchInput" name="search" placeholder="...信息安全">
			<input class="button" type="submit" value="搜索">
		</form>
	</div>
	<script type="text/javascript">
        // 线程 IDs
        var mouseover_tid = [];
        var mouseout_tid = [];
         
        jQuery(document).ready(function(){
            jQuery('body > header > div > nav > div > ul > li.menu-item > ul > li.subitem').each(function(index){
                jQuery('body > header > div > nav > div > ul > li.menu-item').hover(
         
                    // 显示菜单
                    function(){
                        jQuery(this).find('ul:eq(0)').show();
                    },
        
                    // 隐藏菜单
                    function(){
                        jQuery(this).find('ul:eq(0)').hide();
                    }
         
                );
            });
        });
    </script>
    <nav>
        <?php import_part("Custom.module","nav"); ?>
    </nav>
</div>
