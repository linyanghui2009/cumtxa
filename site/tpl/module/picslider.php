<script>
$(document).ready(function(){
    var $picShow=$("#picSlider").find("div.picShow");
    $picShow.find(".item").hover(function(){
        changeTo($(this));
    },function(){

    });
    function changeTo($focusItem){
        $picShow.find(".item").stop().not($focusItem).animate({width:"90px"});
        $focusItem.animate({width:"730px"});
    }
});
</script>

<div class="picShow">
    <div class="item picItem1 present">
        <a href="<?php e_page("article","index",array("type"=>"学生培养","subtype"=>"学科竞赛"));?>">
            <div class="itemPic" style="background-image: url('image/sample.jpg');"></div>
            <div class="picTitle"><span>学科竞赛</span></div>
        </a>
    </div>
    <div class="item picItem2">
        <a href="<?php e_page("article","index",array("type"=>"学生培养","subtype"=>"学生风采"));?>">
            <div class="itemPic" style="background-image: url('image/student1.jpg');"></div>
            <div class="picTitle"><span>学生风采</span></div>
        </a>
    </div>
    <div class="item picItem3">
        <a href="<?php e_page("article","index","class=1");?>">
            <div class="itemPic" style="background-image: url('image/school.jpg');"></div>
            <div class="picTitle"><span>系内新闻公告</span></div>
        </a>
    </div>
    <div class="item picItem4">
        <a href="<?php e_page("article","index","class=2");?>">
            <div class="itemPic" style="background-image: url('image/break.jpg');"></div>
            <div class="picTitle"><span>行业热点新闻</span></div>
        </a>
    </div>
</div>
