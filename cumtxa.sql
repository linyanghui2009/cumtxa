-- phpMyAdmin SQL Dump
-- version 4.2.9.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2014-11-05 05:26:29
-- 服务器版本： 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cumtxa`
--

-- --------------------------------------------------------

--
-- 表的结构 `industry_hot_news`
--

CREATE TABLE IF NOT EXISTS `industry_hot_news` (
`id` int(255) NOT NULL COMMENT '引索',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `content` longtext NOT NULL COMMENT '内容',
  `type` varchar(255) NOT NULL COMMENT '类型',
  `editer` varchar(255) NOT NULL COMMENT '编辑人',
  `date` date NOT NULL COMMENT '编辑日期',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `hot` int(11) DEFAULT NULL COMMENT '热度',
  `views` int(11) NOT NULL COMMENT '浏览次数'
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `industry_hot_news`
--

INSERT INTO `industry_hot_news` (`id`, `title`, `content`, `type`, `editer`, `date`, `create_time`, `hot`, `views`) VALUES
(1, '算不算漏洞？PayPal帐号锁定被绕过引发争议', '安全研究人员&白帽子Kunz Mejri近日发现了一个关于Paypal移动支付API的漏洞，攻击者可以利用该漏洞绕过Paypal的防盗号锁定设计。\r\n利用移动支付API绕过帐号锁定设计\r\nPayPal的防盗号锁定设计是这样的：如果有人多次输入不正确的密码，其PayPal帐户就会被暂时封锁。要解封帐户，用户必须回答一系列的安全提问。\r\n这一安全功能只在常规的Web应用程序中应用，但安全研究人员&白帽子Kunz Mejri发现：移动API不检查账户是否被封，直接允许用户再次登录，\r\nBenjamin Kunz Mejri是 漏洞实验室(Vulnerability Lab)创始人，也是发现该问题的人，他于上周发表了这个漏洞。\r\n“客户端API只会检查帐户是否存在，而不会检查账户是否被封锁，这使得封锁的用户能够访问PayPal账户，并进行转账等交易，他可以送钱从帐户中，iPhone / iPad的Paypal应用需要更新，以确保应用能够验证帐户状态，以防账号盗用的事情发生。”\r\n该漏洞已经过测试，在iOS的应用程序中得到验证，但Kunz Mejri称，Paypal Android版本的应用程序也受到影响。\r\n漏洞证明POC(proof-of-concept)视频：\r\nhttps://www.youtube.com/watch?v=RXubXP_r2M4\r\n', '行业新闻', 'me', '2014-10-21', '2014-10-21 09:16:18', NULL, 3),
(2, '算不算漏洞？PayPal帐号锁定被绕过引发争议', '安全研究人员&白帽子Kunz Mejri近日发现了一个关于Paypal移动支付API的漏洞，攻击者可以利用该漏洞绕过Paypal的防盗号锁定设计。\r\n利用移动支付API绕过帐号锁定设计\r\nPayPal的防盗号锁定设计是这样的：如果有人多次输入不正确的密码，其PayPal帐户就会被暂时封锁。要解封帐户，用户必须回答一系列的安全提问。\r\n这一安全功能只在常规的Web应用程序中应用，但安全研究人员&白帽子Kunz Mejri发现：移动API不检查账户是否被封，直接允许用户再次登录，\r\nBenjamin Kunz Mejri是 漏洞实验室(Vulnerability Lab)创始人，也是发现该问题的人，他于上周发表了这个漏洞。\r\n“客户端API只会检查帐户是否存在，而不会检查账户是否被封锁，这使得封锁的用户能够访问PayPal账户，并进行转账等交易，他可以送钱从帐户中，iPhone / iPad的Paypal应用需要更新，以确保应用能够验证帐户状态，以防账号盗用的事情发生。”\r\n该漏洞已经过测试，在iOS的应用程序中得到验证，但Kunz Mejri称，Paypal Android版本的应用程序也受到影响。\r\n漏洞证明POC(proof-of-concept)视频：\r\nhttps://www.youtube.com/watch?v=RXubXP_r2M4\r\n', '行业新闻', 'me', '2014-10-21', '2014-10-21 09:16:42', NULL, 0),
(3, '算不算漏洞？PayPal帐号锁定被绕过引发争议', '安全研究人员&白帽子Kunz Mejri近日发现了一个关于Paypal移动支付API的漏洞，攻击者可以利用该漏洞绕过Paypal的防盗号锁定设计。\r\n利用移动支付API绕过帐号锁定设计\r\nPayPal的防盗号锁定设计是这样的：如果有人多次输入不正确的密码，其PayPal帐户就会被暂时封锁。要解封帐户，用户必须回答一系列的安全提问。\r\n这一安全功能只在常规的Web应用程序中应用，但安全研究人员&白帽子Kunz Mejri发现：移动API不检查账户是否被封，直接允许用户再次登录，\r\nBenjamin Kunz Mejri是 漏洞实验室(Vulnerability Lab)创始人，也是发现该问题的人，他于上周发表了这个漏洞。\r\n“客户端API只会检查帐户是否存在，而不会检查账户是否被封锁，这使得封锁的用户能够访问PayPal账户，并进行转账等交易，他可以送钱从帐户中，iPhone / iPad的Paypal应用需要更新，以确保应用能够验证帐户状态，以防账号盗用的事情发生。”\r\n该漏洞已经过测试，在iOS的应用程序中得到验证，但Kunz Mejri称，Paypal Android版本的应用程序也受到影响。\r\n漏洞证明POC(proof-of-concept)视频：\r\nhttps://www.youtube.com/watch?v=RXubXP_r2M4\r\n', '行业新闻', 'me', '2014-10-21', '2014-10-21 09:16:42', NULL, 9),
(4, '算不算漏洞？PayPal帐号锁定被绕过引发争议', '安全研究人员&白帽子Kunz Mejri近日发现了一个关于Paypal移动支付API的漏洞，攻击者可以利用该漏洞绕过Paypal的防盗号锁定设计。\r\n利用移动支付API绕过帐号锁定设计\r\nPayPal的防盗号锁定设计是这样的：如果有人多次输入不正确的密码，其PayPal帐户就会被暂时封锁。要解封帐户，用户必须回答一系列的安全提问。\r\n这一安全功能只在常规的Web应用程序中应用，但安全研究人员&白帽子Kunz Mejri发现：移动API不检查账户是否被封，直接允许用户再次登录，\r\nBenjamin Kunz Mejri是 漏洞实验室(Vulnerability Lab)创始人，也是发现该问题的人，他于上周发表了这个漏洞。\r\n“客户端API只会检查帐户是否存在，而不会检查账户是否被封锁，这使得封锁的用户能够访问PayPal账户，并进行转账等交易，他可以送钱从帐户中，iPhone / iPad的Paypal应用需要更新，以确保应用能够验证帐户状态，以防账号盗用的事情发生。”\r\n该漏洞已经过测试，在iOS的应用程序中得到验证，但Kunz Mejri称，Paypal Android版本的应用程序也受到影响。\r\n漏洞证明POC(proof-of-concept)视频：\r\nhttps://www.youtube.com/watch?v=RXubXP_r2M4\r\n', '行业新闻', 'me', '2014-10-21', '2014-10-21 09:16:42', NULL, 2),
(5, '算不算漏洞？PayPal帐号锁定被绕过引发争议', '安全研究人员&白帽子Kunz Mejri近日发现了一个关于Paypal移动支付API的漏洞，攻击者可以利用该漏洞绕过Paypal的防盗号锁定设计。\r\n利用移动支付API绕过帐号锁定设计\r\nPayPal的防盗号锁定设计是这样的：如果有人多次输入不正确的密码，其PayPal帐户就会被暂时封锁。要解封帐户，用户必须回答一系列的安全提问。\r\n这一安全功能只在常规的Web应用程序中应用，但安全研究人员&白帽子Kunz Mejri发现：移动API不检查账户是否被封，直接允许用户再次登录，\r\nBenjamin Kunz Mejri是 漏洞实验室(Vulnerability Lab)创始人，也是发现该问题的人，他于上周发表了这个漏洞。\r\n“客户端API只会检查帐户是否存在，而不会检查账户是否被封锁，这使得封锁的用户能够访问PayPal账户，并进行转账等交易，他可以送钱从帐户中，iPhone / iPad的Paypal应用需要更新，以确保应用能够验证帐户状态，以防账号盗用的事情发生。”\r\n该漏洞已经过测试，在iOS的应用程序中得到验证，但Kunz Mejri称，Paypal Android版本的应用程序也受到影响。\r\n漏洞证明POC(proof-of-concept)视频：\r\nhttps://www.youtube.com/watch?v=RXubXP_r2M4\r\n', '行业新闻', 'me', '2014-10-21', '2014-10-21 09:16:42', NULL, 0),
(6, '算不算漏洞？PayPal帐号锁定被绕过引发争议', '安全研究人员&白帽子Kunz Mejri近日发现了一个关于Paypal移动支付API的漏洞，攻击者可以利用该漏洞绕过Paypal的防盗号锁定设计。\r\n利用移动支付API绕过帐号锁定设计\r\nPayPal的防盗号锁定设计是这样的：如果有人多次输入不正确的密码，其PayPal帐户就会被暂时封锁。要解封帐户，用户必须回答一系列的安全提问。\r\n这一安全功能只在常规的Web应用程序中应用，但安全研究人员&白帽子Kunz Mejri发现：移动API不检查账户是否被封，直接允许用户再次登录，\r\nBenjamin Kunz Mejri是 漏洞实验室(Vulnerability Lab)创始人，也是发现该问题的人，他于上周发表了这个漏洞。\r\n“客户端API只会检查帐户是否存在，而不会检查账户是否被封锁，这使得封锁的用户能够访问PayPal账户，并进行转账等交易，他可以送钱从帐户中，iPhone / iPad的Paypal应用需要更新，以确保应用能够验证帐户状态，以防账号盗用的事情发生。”\r\n该漏洞已经过测试，在iOS的应用程序中得到验证，但Kunz Mejri称，Paypal Android版本的应用程序也受到影响。\r\n漏洞证明POC(proof-of-concept)视频：\r\nhttps://www.youtube.com/watch?v=RXubXP_r2M4\r\n', '行业新闻', 'me', '2014-10-21', '2014-10-21 09:16:42', NULL, 1),
(7, '算不算漏洞？PayPal帐号锁定被绕过引发争议', '安全研究人员&白帽子Kunz Mejri近日发现了一个关于Paypal移动支付API的漏洞，攻击者可以利用该漏洞绕过Paypal的防盗号锁定设计。\r\n利用移动支付API绕过帐号锁定设计\r\nPayPal的防盗号锁定设计是这样的：如果有人多次输入不正确的密码，其PayPal帐户就会被暂时封锁。要解封帐户，用户必须回答一系列的安全提问。\r\n这一安全功能只在常规的Web应用程序中应用，但安全研究人员&白帽子Kunz Mejri发现：移动API不检查账户是否被封，直接允许用户再次登录，\r\nBenjamin Kunz Mejri是 漏洞实验室(Vulnerability Lab)创始人，也是发现该问题的人，他于上周发表了这个漏洞。\r\n“客户端API只会检查帐户是否存在，而不会检查账户是否被封锁，这使得封锁的用户能够访问PayPal账户，并进行转账等交易，他可以送钱从帐户中，iPhone / iPad的Paypal应用需要更新，以确保应用能够验证帐户状态，以防账号盗用的事情发生。”\r\n该漏洞已经过测试，在iOS的应用程序中得到验证，但Kunz Mejri称，Paypal Android版本的应用程序也受到影响。\r\n漏洞证明POC(proof-of-concept)视频：\r\nhttps://www.youtube.com/watch?v=RXubXP_r2M4\r\n', '行业新闻', 'me', '2014-10-21', '2014-10-21 09:16:42', NULL, 2),
(8, '算不算漏洞？PayPal帐号锁定被绕过引发争议', '安全研究人员&白帽子Kunz Mejri近日发现了一个关于Paypal移动支付API的漏洞，攻击者可以利用该漏洞绕过Paypal的防盗号锁定设计。\r\n利用移动支付API绕过帐号锁定设计\r\nPayPal的防盗号锁定设计是这样的：如果有人多次输入不正确的密码，其PayPal帐户就会被暂时封锁。要解封帐户，用户必须回答一系列的安全提问。\r\n这一安全功能只在常规的Web应用程序中应用，但安全研究人员&白帽子Kunz Mejri发现：移动API不检查账户是否被封，直接允许用户再次登录，\r\nBenjamin Kunz Mejri是 漏洞实验室(Vulnerability Lab)创始人，也是发现该问题的人，他于上周发表了这个漏洞。\r\n“客户端API只会检查帐户是否存在，而不会检查账户是否被封锁，这使得封锁的用户能够访问PayPal账户，并进行转账等交易，他可以送钱从帐户中，iPhone / iPad的Paypal应用需要更新，以确保应用能够验证帐户状态，以防账号盗用的事情发生。”\r\n该漏洞已经过测试，在iOS的应用程序中得到验证，但Kunz Mejri称，Paypal Android版本的应用程序也受到影响。\r\n漏洞证明POC(proof-of-concept)视频：\r\nhttps://www.youtube.com/watch?v=RXubXP_r2M4\r\n', '行业新闻', 'me', '2014-10-21', '2014-10-21 09:16:42', NULL, 1),
(9, '算不算漏洞？PayPal帐号锁定被绕过引发争议', '安全研究人员&白帽子Kunz Mejri近日发现了一个关于Paypal移动支付API的漏洞，攻击者可以利用该漏洞绕过Paypal的防盗号锁定设计。\r\n利用移动支付API绕过帐号锁定设计\r\nPayPal的防盗号锁定设计是这样的：如果有人多次输入不正确的密码，其PayPal帐户就会被暂时封锁。要解封帐户，用户必须回答一系列的安全提问。\r\n这一安全功能只在常规的Web应用程序中应用，但安全研究人员&白帽子Kunz Mejri发现：移动API不检查账户是否被封，直接允许用户再次登录，\r\nBenjamin Kunz Mejri是 漏洞实验室(Vulnerability Lab)创始人，也是发现该问题的人，他于上周发表了这个漏洞。\r\n“客户端API只会检查帐户是否存在，而不会检查账户是否被封锁，这使得封锁的用户能够访问PayPal账户，并进行转账等交易，他可以送钱从帐户中，iPhone / iPad的Paypal应用需要更新，以确保应用能够验证帐户状态，以防账号盗用的事情发生。”\r\n该漏洞已经过测试，在iOS的应用程序中得到验证，但Kunz Mejri称，Paypal Android版本的应用程序也受到影响。\r\n漏洞证明POC(proof-of-concept)视频：\r\nhttps://www.youtube.com/watch?v=RXubXP_r2M4\r\n', '行业新闻', 'me', '2014-10-21', '2014-10-21 09:16:42', NULL, 4),
(10, '算不算漏洞？PayPal帐号锁定被绕过引发争议', '安全研究人员&白帽子Kunz Mejri近日发现了一个关于Paypal移动支付API的漏洞，攻击者可以利用该漏洞绕过Paypal的防盗号锁定设计。\r\n利用移动支付API绕过帐号锁定设计\r\nPayPal的防盗号锁定设计是这样的：如果有人多次输入不正确的密码，其PayPal帐户就会被暂时封锁。要解封帐户，用户必须回答一系列的安全提问。\r\n这一安全功能只在常规的Web应用程序中应用，但安全研究人员&白帽子Kunz Mejri发现：移动API不检查账户是否被封，直接允许用户再次登录，\r\nBenjamin Kunz Mejri是 漏洞实验室(Vulnerability Lab)创始人，也是发现该问题的人，他于上周发表了这个漏洞。\r\n“客户端API只会检查帐户是否存在，而不会检查账户是否被封锁，这使得封锁的用户能够访问PayPal账户，并进行转账等交易，他可以送钱从帐户中，iPhone / iPad的Paypal应用需要更新，以确保应用能够验证帐户状态，以防账号盗用的事情发生。”\r\n该漏洞已经过测试，在iOS的应用程序中得到验证，但Kunz Mejri称，Paypal Android版本的应用程序也受到影响。\r\n漏洞证明POC(proof-of-concept)视频：\r\nhttps://www.youtube.com/watch?v=RXubXP_r2M4\r\n', '行业新闻', 'me', '2014-10-21', '2014-10-21 09:16:42', NULL, 2),
(11, '算不算漏洞？PayPal帐号锁定被绕过引发争议', '安全研究人员&白帽子Kunz Mejri近日发现了一个关于Paypal移动支付API的漏洞，攻击者可以利用该漏洞绕过Paypal的防盗号锁定设计。\r\n利用移动支付API绕过帐号锁定设计\r\nPayPal的防盗号锁定设计是这样的：如果有人多次输入不正确的密码，其PayPal帐户就会被暂时封锁。要解封帐户，用户必须回答一系列的安全提问。\r\n这一安全功能只在常规的Web应用程序中应用，但安全研究人员&白帽子Kunz Mejri发现：移动API不检查账户是否被封，直接允许用户再次登录，\r\nBenjamin Kunz Mejri是 漏洞实验室(Vulnerability Lab)创始人，也是发现该问题的人，他于上周发表了这个漏洞。\r\n“客户端API只会检查帐户是否存在，而不会检查账户是否被封锁，这使得封锁的用户能够访问PayPal账户，并进行转账等交易，他可以送钱从帐户中，iPhone / iPad的Paypal应用需要更新，以确保应用能够验证帐户状态，以防账号盗用的事情发生。”\r\n该漏洞已经过测试，在iOS的应用程序中得到验证，但Kunz Mejri称，Paypal Android版本的应用程序也受到影响。\r\n漏洞证明POC(proof-of-concept)视频：\r\nhttps://www.youtube.com/watch?v=RXubXP_r2M4\r\n', '行业新闻', 'me', '2014-10-21', '2014-10-21 09:16:42', NULL, 1),
(12, '算不算漏洞？PayPal帐号锁定被绕过引发争议', '安全研究人员&白帽子Kunz Mejri近日发现了一个关于Paypal移动支付API的漏洞，攻击者可以利用该漏洞绕过Paypal的防盗号锁定设计。\r\n利用移动支付API绕过帐号锁定设计\r\nPayPal的防盗号锁定设计是这样的：如果有人多次输入不正确的密码，其PayPal帐户就会被暂时封锁。要解封帐户，用户必须回答一系列的安全提问。\r\n这一安全功能只在常规的Web应用程序中应用，但安全研究人员&白帽子Kunz Mejri发现：移动API不检查账户是否被封，直接允许用户再次登录，\r\nBenjamin Kunz Mejri是 漏洞实验室(Vulnerability Lab)创始人，也是发现该问题的人，他于上周发表了这个漏洞。\r\n“客户端API只会检查帐户是否存在，而不会检查账户是否被封锁，这使得封锁的用户能够访问PayPal账户，并进行转账等交易，他可以送钱从帐户中，iPhone / iPad的Paypal应用需要更新，以确保应用能够验证帐户状态，以防账号盗用的事情发生。”\r\n该漏洞已经过测试，在iOS的应用程序中得到验证，但Kunz Mejri称，Paypal Android版本的应用程序也受到影响。\r\n漏洞证明POC(proof-of-concept)视频：\r\nhttps://www.youtube.com/watch?v=RXubXP_r2M4\r\n', '行业新闻', 'me', '2014-10-21', '2014-10-21 09:16:42', NULL, 0),
(13, '算不算漏洞？PayPal帐号锁定被绕过引发争议', '安全研究人员&白帽子Kunz Mejri近日发现了一个关于Paypal移动支付API的漏洞，攻击者可以利用该漏洞绕过Paypal的防盗号锁定设计。\r\n利用移动支付API绕过帐号锁定设计\r\nPayPal的防盗号锁定设计是这样的：如果有人多次输入不正确的密码，其PayPal帐户就会被暂时封锁。要解封帐户，用户必须回答一系列的安全提问。\r\n这一安全功能只在常规的Web应用程序中应用，但安全研究人员&白帽子Kunz Mejri发现：移动API不检查账户是否被封，直接允许用户再次登录，\r\nBenjamin Kunz Mejri是 漏洞实验室(Vulnerability Lab)创始人，也是发现该问题的人，他于上周发表了这个漏洞。\r\n“客户端API只会检查帐户是否存在，而不会检查账户是否被封锁，这使得封锁的用户能够访问PayPal账户，并进行转账等交易，他可以送钱从帐户中，iPhone / iPad的Paypal应用需要更新，以确保应用能够验证帐户状态，以防账号盗用的事情发生。”\r\n该漏洞已经过测试，在iOS的应用程序中得到验证，但Kunz Mejri称，Paypal Android版本的应用程序也受到影响。\r\n漏洞证明POC(proof-of-concept)视频：\r\nhttps://www.youtube.com/watch?v=RXubXP_r2M4\r\n', '行业新闻', 'me', '2014-10-21', '2014-10-21 09:16:42', NULL, 0),
(14, '算不算漏洞？PayPal帐号锁定被绕过引发争议', '安全研究人员&白帽子Kunz Mejri近日发现了一个关于Paypal移动支付API的漏洞，攻击者可以利用该漏洞绕过Paypal的防盗号锁定设计。\r\n利用移动支付API绕过帐号锁定设计\r\nPayPal的防盗号锁定设计是这样的：如果有人多次输入不正确的密码，其PayPal帐户就会被暂时封锁。要解封帐户，用户必须回答一系列的安全提问。\r\n这一安全功能只在常规的Web应用程序中应用，但安全研究人员&白帽子Kunz Mejri发现：移动API不检查账户是否被封，直接允许用户再次登录，\r\nBenjamin Kunz Mejri是 漏洞实验室(Vulnerability Lab)创始人，也是发现该问题的人，他于上周发表了这个漏洞。\r\n“客户端API只会检查帐户是否存在，而不会检查账户是否被封锁，这使得封锁的用户能够访问PayPal账户，并进行转账等交易，他可以送钱从帐户中，iPhone / iPad的Paypal应用需要更新，以确保应用能够验证帐户状态，以防账号盗用的事情发生。”\r\n该漏洞已经过测试，在iOS的应用程序中得到验证，但Kunz Mejri称，Paypal Android版本的应用程序也受到影响。\r\n漏洞证明POC(proof-of-concept)视频：\r\nhttps://www.youtube.com/watch?v=RXubXP_r2M4\r\n', '行业新闻', 'me', '2014-10-21', '2014-10-21 09:16:42', NULL, 0),
(15, '算不算漏洞？PayPal帐号锁定被绕过引发争议', '安全研究人员&白帽子Kunz Mejri近日发现了一个关于Paypal移动支付API的漏洞，攻击者可以利用该漏洞绕过Paypal的防盗号锁定设计。\r\n利用移动支付API绕过帐号锁定设计\r\nPayPal的防盗号锁定设计是这样的：如果有人多次输入不正确的密码，其PayPal帐户就会被暂时封锁。要解封帐户，用户必须回答一系列的安全提问。\r\n这一安全功能只在常规的Web应用程序中应用，但安全研究人员&白帽子Kunz Mejri发现：移动API不检查账户是否被封，直接允许用户再次登录，\r\nBenjamin Kunz Mejri是 漏洞实验室(Vulnerability Lab)创始人，也是发现该问题的人，他于上周发表了这个漏洞。\r\n“客户端API只会检查帐户是否存在，而不会检查账户是否被封锁，这使得封锁的用户能够访问PayPal账户，并进行转账等交易，他可以送钱从帐户中，iPhone / iPad的Paypal应用需要更新，以确保应用能够验证帐户状态，以防账号盗用的事情发生。”\r\n该漏洞已经过测试，在iOS的应用程序中得到验证，但Kunz Mejri称，Paypal Android版本的应用程序也受到影响。\r\n漏洞证明POC(proof-of-concept)视频：\r\nhttps://www.youtube.com/watch?v=RXubXP_r2M4\r\n', '行业新闻', 'me', '2014-10-21', '2014-10-21 09:16:42', NULL, 0),
(16, '算不算漏洞？PayPal帐号锁定被绕过引发争议', '安全研究人员&白帽子Kunz Mejri近日发现了一个关于Paypal移动支付API的漏洞，攻击者可以利用该漏洞绕过Paypal的防盗号锁定设计。\r\n利用移动支付API绕过帐号锁定设计\r\nPayPal的防盗号锁定设计是这样的：如果有人多次输入不正确的密码，其PayPal帐户就会被暂时封锁。要解封帐户，用户必须回答一系列的安全提问。\r\n这一安全功能只在常规的Web应用程序中应用，但安全研究人员&白帽子Kunz Mejri发现：移动API不检查账户是否被封，直接允许用户再次登录，\r\nBenjamin Kunz Mejri是 漏洞实验室(Vulnerability Lab)创始人，也是发现该问题的人，他于上周发表了这个漏洞。\r\n“客户端API只会检查帐户是否存在，而不会检查账户是否被封锁，这使得封锁的用户能够访问PayPal账户，并进行转账等交易，他可以送钱从帐户中，iPhone / iPad的Paypal应用需要更新，以确保应用能够验证帐户状态，以防账号盗用的事情发生。”\r\n该漏洞已经过测试，在iOS的应用程序中得到验证，但Kunz Mejri称，Paypal Android版本的应用程序也受到影响。\r\n漏洞证明POC(proof-of-concept)视频：\r\nhttps://www.youtube.com/watch?v=RXubXP_r2M4\r\n', '行业新闻', 'me', '2014-10-21', '2014-10-21 09:16:42', NULL, 0),
(17, '长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长', '长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长', '1', '1', '2014-11-02', '2014-11-02 05:16:57', NULL, 1);

-- --------------------------------------------------------

--
-- 表的结构 `news`
--

CREATE TABLE IF NOT EXISTS `news` (
`id` int(255) NOT NULL COMMENT '引索',
  `type` varchar(255) NOT NULL COMMENT '类型',
  `subtype` varchar(255) DEFAULT NULL COMMENT '次级分类',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `content` longtext NOT NULL COMMENT '内容',
  `editer` varchar(255) DEFAULT NULL COMMENT '编辑人',
  `date` date NOT NULL COMMENT '编辑时间',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `views` int(11) NOT NULL DEFAULT '0' COMMENT '浏览次数',
  `hot` int(11) DEFAULT NULL COMMENT '热度'
) ENGINE=InnoDB AUTO_INCREMENT=151 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `news`
--

INSERT INTO `news` (`id`, `type`, `subtype`, `title`, `content`, `editer`, `date`, `create_time`, `views`, `hot`) VALUES
(1, '网站信息', NULL, '联系我们', '联系我们', NULL, '2014-11-02', '2014-11-02 04:56:54', 3, NULL),
(144, '网站信息', NULL, '计算机学院信息安全系', '内容改为：信息安全系主任、书记办公室    A315-2   \r\n\r\n电话：（0516）83591723\r\n\r\n通信地址：  江苏省徐州市中国矿业大学计算机学院信息安全系\r\n\r\n邮编：221116', NULL, '2014-11-02', '2014-11-02 04:53:00', 9, NULL),
(145, '学生培养', '学科竞赛', '123', '123', NULL, '2014-11-02', '2014-11-02 05:23:44', 0, NULL),
(147, '首页专区', '首页专区', '首页首页', '<p>首页首页</p>', '首页首页', '2014-11-02', '2014-11-04 08:56:55', 1, NULL),
(148, '网站信息', '专业概述', '专业概述', '<p>专业概述</p>', '首页首页', '2014-11-02', '2014-11-04 08:58:50', 2, NULL),
(149, '首页专区', '首页专区', '1 2', '<p>1</p><p>2</p><p><br/></p>', '312', '2023-12-03', '2014-11-04 09:07:09', 1, NULL),
(150, '网站信息', '专业概述', '专业介绍', '<p>。。。。</p>', '信安', '2014-11-02', '2014-11-04 09:12:46', 5, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `news_subtype`
--

CREATE TABLE IF NOT EXISTS `news_subtype` (
`id` int(255) NOT NULL COMMENT '引索',
  `name` varchar(255) NOT NULL COMMENT '次级分类名称',
  `pretype` varchar(255) DEFAULT NULL COMMENT '上级分类'
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `news_subtype`
--

INSERT INTO `news_subtype` (`id`, `name`, `pretype`) VALUES
(11, '学科竞赛', '学生培养'),
(12, '学生风采', '学生培养'),
(13, '就业指南', '学生培养'),
(14, '学习规划', '学生培养'),
(15, '培养计划', '学科建设'),
(16, '课程建设', '学科建设'),
(17, '专业概述', '网站信息'),
(18, '首页专区', '首页专区');

-- --------------------------------------------------------

--
-- 表的结构 `news_type`
--

CREATE TABLE IF NOT EXISTS `news_type` (
`id` int(255) NOT NULL COMMENT '引索',
  `name` varchar(255) NOT NULL COMMENT '分类名'
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `news_type`
--

INSERT INTO `news_type` (`id`, `name`) VALUES
(1, '专业概述'),
(8, '网站信息'),
(9, '学生培养'),
(10, '学科建设'),
(11, '行业新闻');

-- --------------------------------------------------------

--
-- 表的结构 `user_admin`
--

CREATE TABLE IF NOT EXISTS `user_admin` (
`id` int(11) NOT NULL COMMENT '引索',
  `username` varchar(255) NOT NULL COMMENT '用户名',
  `password` varchar(255) NOT NULL COMMENT '密码',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '权限等级'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `user_admin`
--

INSERT INTO `user_admin` (`id`, `username`, `password`, `status`) VALUES
(2, 'admin', 'f31213e8abc95eef9f78d55bfabbc19073fe637e', 1);

-- --------------------------------------------------------

--
-- 表的结构 `user_student`
--

CREATE TABLE IF NOT EXISTS `user_student` (
`id` int(255) NOT NULL COMMENT '引索',
  `username` varchar(255) NOT NULL COMMENT '用户名',
  `password` int(255) NOT NULL COMMENT '密码',
  `sex` set('male','female') DEFAULT NULL COMMENT '性别',
  `nickname` int(255) DEFAULT NULL COMMENT '昵称',
  `viewtime` time DEFAULT '00:00:00' COMMENT '在线时长'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `user_teacher`
--

CREATE TABLE IF NOT EXISTS `user_teacher` (
`id` int(255) NOT NULL COMMENT '引索',
  `username` varchar(255) NOT NULL COMMENT '用户名',
  `password` varchar(255) NOT NULL COMMENT '密码',
  `name` varchar(255) NOT NULL COMMENT '姓名'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `industry_hot_news`
--
ALTER TABLE `industry_hot_news`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_subtype`
--
ALTER TABLE `news_subtype`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_type`
--
ALTER TABLE `news_type`
 ADD PRIMARY KEY (`id`), ADD KEY `id` (`id`);

--
-- Indexes for table `user_admin`
--
ALTER TABLE `user_admin`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_student`
--
ALTER TABLE `user_student`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_teacher`
--
ALTER TABLE `user_teacher`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `industry_hot_news`
--
ALTER TABLE `industry_hot_news`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT COMMENT '引索',AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT COMMENT '引索',AUTO_INCREMENT=151;
--
-- AUTO_INCREMENT for table `news_subtype`
--
ALTER TABLE `news_subtype`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT COMMENT '引索',AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `news_type`
--
ALTER TABLE `news_type`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT COMMENT '引索',AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `user_admin`
--
ALTER TABLE `user_admin`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '引索',AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user_student`
--
ALTER TABLE `user_student`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT COMMENT '引索';
--
-- AUTO_INCREMENT for table `user_teacher`
--
ALTER TABLE `user_teacher`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT COMMENT '引索';
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
